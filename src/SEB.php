<?php

namespace App\Sources\Seb;


use Finpaa\Finpaa;

class Seb
{
    // Sequence Codes
    private static $redirectAuth;
    private static $decoupleAuth;
    private static $ais;
    private static $pisDomesticPrivate;
    // private static $pisCrossBorder;

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();

        $sequences = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->SEB();

        self::$redirectAuth = $sequences->SEB_Redirect_Auth();
        // self::$decoupleAuth = $sequences->SEB_Decouple_Auth();
        self::$ais = $sequences->SEB_AIS();
        self::$pisDomesticPrivate = $sequences->SEB_PIS_Domestic_Private();
        // self::$pisCrossBorder = $sequences->ICA_PIS_CrossBorder();
    }

    private static function getSequenceCode($name) {
      if(self::$$name) {
        return self::$$name;
      }
      else {
        self::selfConstruct();
        return self::getSequenceCode($name);
      }
    }

    private static function executeSequenceMethod($code, $methodIndex, $alterations, $name, $returnPayload)
    {
        $sequence = Finpaa::getSequenceMethods($code);

        if (isset($sequence->SequenceExecutions)) {

          if($methodIndex < count($sequence->SequenceExecutions))
          {
            $response = Finpaa::executeTheMethod(
              $sequence->SequenceExecutions[$methodIndex]->methodCode, $alterations, $returnPayload
            );

            return array('error' => false, 'response' => json_decode(json_encode($response), true));
          }
          else {
              return array('error' => true, 'message' => $name . ' method Failed -> Index out of bound',
                'methodIndex' => $methodIndex, 'methodsCount' => count($sequence->SequenceExecutions)
              );
          }
        }
        else {
          return array('error' => true,
            'message' => $name .' -> No sequence executions', 'response' => json_decode(json_encode($sequence), true)
          );
       }
    }

    public static function redirectAuthorize($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('redirectAuth'), $methodIndex, $alterations, 'SEB Redirect Auth', $returnPayload
        );
    }
    public static function decoupleAuthorize($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('decoupleAuth'), $methodIndex, $alterations, 'SEB Decouple Auth', $returnPayload
        );
    }
    public static function AIS($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('ais'), $methodIndex, $alterations, 'SEB AIS', $returnPayload
        );
    }
    public static function PisDomesticPrivate($methodIndex, $alterations = [], $returnPayload = false)
    {
        return self::executeSequenceMethod(
          self::getSequenceCode('pisDomesticPrivate'), $methodIndex, $alterations, 'SEB PIS Domestic Private', $returnPayload
        );
    }
    // public static function PisCrossBorder($methodIndex, $alterations = [], $returnPayload = false)
    // {
    //     return self::executeSequenceMethod(
    //       self::getSequenceCode('pisCrossBorder'), $methodIndex, $alterations, 'SEB PIS Cross Border', $returnPayload
    //     );
    // }
}
